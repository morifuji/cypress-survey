it("awesome e2e test", () => {
    cy.visit("https://blog.morifuji-is.ninja/")
  
    cy.document().then((doc) => {
      const articleList = doc.querySelectorAll("article")

      articleList.forEach((_, index) => {
        cy.get("article").eq(index).find(".post-title").click()
        cy.get("h1").should('not.be.empty')
  
        // 一覧へ戻る
        cy.visit("https://blog.morifuji-is.ninja/")
      })
    })
  })