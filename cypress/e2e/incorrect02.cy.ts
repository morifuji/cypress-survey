it("awesome e2e test", () => {
    cy.visit("https://blog.morifuji-is.ninja/")
  
    const children = cy.get('article').children
    for (let index = 0; index < children.length; index++) {
      // 詳細画面へ遷移
      cy.get('article').eq(index).find("a").click()
      cy.get("h1").should('not.be.empty')
  
      // 一覧へ戻る
      cy.visit("https://blog.morifuji-is.ninja/")
    }
  })