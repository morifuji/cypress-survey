it("awesome e2e test", () => {
    cy.visit("https://blog.morifuji-is.ninja/")
  
    cy.get('article').each(($el, index, $list) => {
      // 詳細画面へ遷移
      cy.wrap($el).find("a").click()
      cy.get("h1").should('not.be.empty')
  
      // 一覧へ戻る
      cy.visit("https://blog.morifuji-is.ninja/")
    })
  })